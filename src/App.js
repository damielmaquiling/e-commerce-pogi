import { BrowserRouter, Routes, Route } from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import Footer from "./components/Footer";
import { useAuthContext } from "./hooks/useAuthContext";
import {
  Home,
  Login,
  Register,
  Dashboard,
  Products,
  Error,
  ProductInfo,
  Cart,
  Profile,
} from "./pages";

function App() {
  const { user } = useAuthContext();
  return (
    <BrowserRouter>
      <AppNavbar />
      <Routes>
        <Route
          path="/"
          element={user && user.isAdmin ? <Dashboard /> : <Home />}
        />
        <Route path="/login" element={<Login />} />
        <Route
          path="/register"
          element={
            (user && user.isAdmin) || (user && user) ? <Error /> : <Register />
          }
        />
        <Route
          path="/products"
          element={user && user.isAdmin ? <Error /> : <Products />}
        />
        <Route path="/cart" element={user && user ? <Cart /> : <Home />} />
        <Route
          path="/profile"
          element={
            (user && user.isAdmin) || (user && user) ? <Profile /> : <Error />
          }
        />

        <Route
          path="/products/:id"
          element={user && user.isAdmin ? <Error /> : <ProductInfo />}
        />
        <Route path="*" element={<Error />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
