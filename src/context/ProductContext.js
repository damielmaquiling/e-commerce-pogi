import { createContext, useEffect, useReducer } from "react";
import { useAuthContext } from "../hooks/useAuthContext";

export const ProductContext = createContext();

export const productsReducer = (state, action) => {
  switch (action.type) {
    case "SET_PRODUCTS":
      return {
        products: action.payload,
      };
    case "CREATE_PRODUCTS":
      return {
        products: [action.payload, ...state.products],
      };
    case "UPDATE_PRODUCTS":
      const updatedProductIndex = state.products.findIndex(
        (product) => product._ud === action.payload._id
      );

      if (updatedProductIndex === -1) {
        return state;
      }

      const updatedProducts = [...state.product];
      updatedProducts[updatedProductIndex] = action.payload;
      return {
        ...state,
        products: updatedProducts,
      };
    case "FETCH_PRODUCTS":
      const fetchProducts = async () => {
        if (action.payload.user && action.payload.user.token) {
          const response = await fetch("http://localhost:5001/products/", {
            method: "GET",
            headers: {
              Authorization: `Bearer ${action.payload.user.token}`,
            },
          });

          const json = await response.json();
          if (response.ok) {
            action.payload.dispatch({ type: "SET_PRODUCTS", payload: json });
          }
        }
      };
      fetchProducts();
      return state;

    default:
      return state;
  }
};

export const ProductContextProvider = ({ children }) => {
  const { user } = useAuthContext();

  const [state, dispatch] = useReducer(productsReducer, {
    products: null,
  });

  return (
    <ProductContext.Provider value={{ ...state, dispatch }}>
      {children}
    </ProductContext.Provider>
  );
};
