import { useEffect, useState } from "react";
import { useProductContext } from "../hooks/useProductContext";
import { useAuthContext } from "../hooks/useAuthContext";
import ProductDetails from "../components/ProductDetails";
import ProductForm from "../components/ProductForm";
import AdminSidebar from "../components/AdminSidebar";
import { Col, Row } from "react-bootstrap";

const Dashboard = () => {
  const { products, dispatch } = useProductContext();
  const { user } = useAuthContext();
  const [show, setShow] = useState(true);

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${user.token}`,
          },
        }
      );

      const json = await response.json();
      if (response.ok) {
        dispatch({ type: "SET_PRODUCTS", payload: json });
      }
    };
    fetchProducts();
    // eslint-disable-next-line
  }, [products]);

  return (
    <div className="home">
      <Row>
        <Col className="col-2">
          <AdminSidebar />
        </Col>
        <Col className="col-7">
          <div className="products">
            {products &&
              products.map((product) => (
                <ProductDetails key={product._id} product={product} />
              ))}
          </div>
        </Col>
        <Col className="col-3">
          <ProductForm />
        </Col>
      </Row>
    </div>
  );
};

export default Dashboard;
