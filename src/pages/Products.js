import { useEffect } from "react";
import { Container, Row } from "react-bootstrap";
import ActiveProductsDetails from "../components/ActiveProductsDetails";
import { useAuthContext } from "../hooks/useAuthContext";
import { useProductContext } from "../hooks/useProductContext";

const Products = () => {
  const { products, dispatch } = useProductContext();
  const { user } = useAuthContext();


  useEffect(() => {
    window.scrollTo(0, 0);
    const fetchActiveProducts = async () => {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`);

      const json = await response.json();
      if (response.ok) {
        dispatch({ type: "SET_PRODUCTS", payload: json });
      }
    };

    fetchActiveProducts();
  }, [dispatch]);

  return (
    <>
      <div className="banner">
        <h1>All Products</h1>
      </div>

      <Container className="product">
        <Row>
          {products &&
            products.map((product) => (
              <ActiveProductsDetails key={product._id} product={product} />
            ))}
        </Row>
      </Container>
    </>
  );
};

export default Products;
