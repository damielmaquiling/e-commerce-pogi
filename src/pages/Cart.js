import { useEffect } from "react";
import { Button, Col, Container, Row, Table } from "react-bootstrap";
import { useUserCart } from "../hooks/useUserCart";
import { useAuthContext } from "../hooks/useAuthContext";
import CartTable from "../components/CartTable";
import { NavLink } from "react-router-dom";
import { useCheckout } from "../hooks/useCheckout";

const Cart = () => {
  const { orders, setOrders, totalAmount, setTotalAmount, userCart } =
    useUserCart();
  const { user } = useAuthContext();
  const checkout = useCheckout();

  useEffect(() => {
    userCart();
  }, []);

  useEffect(() => {
    // recalculate the total amount of the cart
    const newTotalAmount = orders.reduce(
      (total, order) =>
        total +
        order.reduce(
          (orderTotal, item) => orderTotal + item.price * item.quantity,
          0
        ),
      0
    );
    setTotalAmount(newTotalAmount);
  }, [orders]);

  const handleTableRender = (newQuantity, productId) => {
    // find the item with the matching productId and update its quantity
    const updatedOrders = orders.map(
      (order) =>
        order
          .map((item) =>
            item.productId === productId
              ? { ...item, quantity: newQuantity }
              : item
          )
          .filter((item) => item.quantity !== 0) // remove the item if the new quantity is 0
    );

    // update the orders state
    setOrders(updatedOrders);
  };

  const chekoutHandler = async () => {
    await checkout();
    setOrders([]);
  };

  return (
    <Container fluid className="cart">
      <Container>
        <h1>Shopping Cart</h1>

        <CartTable
          orders={orders}
          totalAmount={totalAmount}
          handleTableRender={handleTableRender}
        />

        <div className="check-out">
          <h3>
            <span>Subtotal</span>&#8369; {totalAmount}
          </h3>
          <p>
            {totalAmount === 0
              ? `Bat di ka pa bumibili!, Gigil ako sayo,
            GO!`
              : `Bat di ka pa mag checkout, Gigil ako sayo, Para kang di nag Grade 2,
            GO!`}
          </p>
          <NavLink
            to="/products"
            className="mx-0"
            hidden={totalAmount === 0 ? false : true}
          >
            <Button className="dark-primary-btn">Procceed to products</Button>
          </NavLink>
          <Button
            className={
              totalAmount === 0
                ? "dark-primary-btn-disable"
                : "dark-primary-btn"
            }
            hidden={totalAmount === 0 ? true : false}
            onClick={chekoutHandler}
          >
            Procceed to checkout
          </Button>
        </div>
      </Container>
    </Container>
  );
};

export default Cart;
