import Home from "./Home";
import Login from "./Login";
import Register from "./Register";
import Dashboard from "./Dashboard";
import Products from "./Products";
import Error from "./Error";
import ProductInfo from "./ProductInfo";
import Cart from "./Cart";
import Profile from "./Profile";

export { Home, Profile, Login, Register, Dashboard, Products, Error, ProductInfo, Cart };
