import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useUserOrderHistory } from "../hooks/useUserOrderHistory";
import { useUserDetails } from "../hooks/useUserDetails";
import profileImg from "../images/profile-img.png";
import HistoryCartTable from "../components/HistoryCartTable";
import EditProfileModal from "../components/EditProfileModal";
import UpdatePasswordModal from "../components/UpdatePasswordModal";

const Profile = () => {
  const { userDetails, userDetailsResult, userJoined } = useUserDetails();


  const [show, setShow] = useState(false);
  const [showPassword, setShowPassword] = useState(false)

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const handleShowPassword = () => setShowPassword(true);
  const handleClosePassword = () => setShowPassword(false);


  const { userOrderHistory, orders } = useUserOrderHistory();

  useEffect(() => {
    userOrderHistory();
    userDetails();
  }, [show]);

  return (
    <Container className="profile-details">
      <Row>
        <Col className="profile-details-info col-3">
          <img src={profileImg} alt="profile-img" />
          <h4>Upload a photo</h4>
          <p>`Email: {userDetailsResult.email}`</p>
          <p>`Mobile Number: {userDetailsResult.mobileNum}`</p>
          <Button className="dark-primary-btn" onClick={handleShow}>
            Edit Profile
          </Button>
          <Button className="dark-primary-btn-disable" onClick={handleShowPassword}>Change Password</Button>
        </Col>
        <Col className="profile-details-history col-9">
          <h1>{`Hello, ${userDetailsResult.firstName} ${userDetailsResult.lastName}`}</h1>
          <p>
            `Joined at{" "}
            {userJoined}`
          </p>

          <h2>Order History</h2>
          <HistoryCartTable orders={orders} />
        </Col>
      </Row>

      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header closeButton className="p-4">
          <Modal.Title>
            <h2>Edit profile</h2>
          </Modal.Title>
        </Modal.Header>
        <EditProfileModal userDetailsResult={userDetailsResult} handleClose={handleClose}/>
      </Modal>

      <Modal show={showPassword} onHide={handleClosePassword} size="lg">
        <Modal.Header closeButton className="p-4">
          <Modal.Title>
            <h2>Update Password</h2>
          </Modal.Title>
        </Modal.Header>
        <UpdatePasswordModal handleClosePassword={handleClosePassword}/>
      </Modal>
    </Container>
  );
};

export default Profile;
