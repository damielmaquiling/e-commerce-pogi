import { useEffect } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import BrandOffer from "../components/BrandOffer";
import Footer from "../components/Footer";
import NewProducts from "../components/NewProducts";
import NewsLetter from "../components/NewsLetter";

const Home = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <main>
        <Container fluid>
          <Row className="header-banner">
            <Col className="header-details col-8">
              <h1>
                Unlock Your Best Look: Essential Skincare Products for Men
              </h1>
              <NavLink to="/products">
                <Button className="primary-btn">View collection</Button>
              </NavLink>

              <p>
                Maximize Your Poginess with These Top Skincare Products and the
                Ultimate Routine for Men: Achieve a Clear, Smooth, and Youthful
                Complexion from Cleansers to Moisturizers.
              </p>
            </Col>
            <Col className="header-img col-4"></Col>
          </Row>
        </Container>
      </main>
      <BrandOffer />

      <Container className="new-products-title">
        <h2>New products</h2>
      </Container>
      <NewProducts />
      <NewsLetter />
      <Footer />
    </>
  );
};

export default Home;
