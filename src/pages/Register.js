import { useState, useEffect} from "react";
import { Form, Button, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";

import { useRegister } from "../hooks/useRegister";

const Register = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  const { register, successRegister } = useRegister();

  useEffect(() => {
    if (
      email !== "" &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, firstName, lastName, mobileNo, password1, password2]);

  const registerUser = async (e) => {
    e.preventDefault();

    const details = { email, firstName, lastName, mobileNo, password1 };

    await register(details);

    if (successRegister) {
      setFirstName("");
      setLastName("");
      setMobileNo("");
      setEmail("");
      setPassword1("");
      setPassword2("");
    }
  };

  return successRegister ? (
    <Navigate to="/login" />
  ) : (
    <Container>
      <Form onSubmit={registerUser}>
        <Form.Group className="mb-3" controlId="firstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter first name"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter last name"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="mobileNo">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Mobile Number"
            value={mobileNo}
            maxLength="11"
            onChange={(e) => setMobileNo(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Verify Password"
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
            required
          />
        </Form.Group>

        <Button
          variant={isActive ? "success " : "danger"}
          type="submit"
          id="submitBtn"
          disabled={isActive ? false : true}
        >
          Submit
        </Button>
      </Form>
    </Container>
  );
};

export default Register;
