import { useEffect, useState } from "react";

import { Form, Button, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";

import { useAuthContext } from "../hooks/useAuthContext";
import { useLogin } from "../hooks/useLogin";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const login = useLogin();
  const { user } = useAuthContext();
  
  useEffect(() => {
    if (email && password) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);
  
  const authenticate = async (e) => {
    e.preventDefault();

    await login(email, password);
  };


  return user !== null ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Form onSubmit={authenticate}>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        <Button
          variant={isActive ? "success " : "danger"}
          type="submit"
          id="submitBtn"
          disabled={isActive ? false : true}
        >
          Submit
        </Button>
      </Form>
    </Container>
  );
};

export default Login;
