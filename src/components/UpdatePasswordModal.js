import { useState, useEffect } from "react";
import { Form, Button, Container, FloatingLabel } from "react-bootstrap";

import { useUpdatePassword } from "../hooks/useUpdatePassword";

const UpdatePasswordModal = ({ handleClosePassword }) => {
  const UpdatePassword = useUpdatePassword();

  const [password, setPassword] = useState("");
  const [passwordVerify, setPasswordVerify] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (password && passwordVerify && password === passwordVerify) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [password, passwordVerify]);

  const updateHandle = async (e) => {
    e.preventDefault();
    await UpdatePassword(password)
    handleClosePassword();
  };
  return (
    <Container>
      <Form onSubmit={updateHandle} className="p-3 my-3">
        <Form.Group className="mb-3">
          <FloatingLabel label="Password" className="mb-3">
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Form.Group className="mb-3">
          <FloatingLabel label="Verify Password" className="mb-3">
            <Form.Control
              type="password"
              placeholder="Verify Password"
              onChange={(e) => setPasswordVerify(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Button
          variant={isActive ? "success " : "danger"}
          type="submit"
          id="submitBtn"
          disabled={isActive ? false : true}
          className="dark-primary-btn mt-3"
        >
          Update
        </Button>
      </Form>
    </Container>
  );
};

export default UpdatePasswordModal;
