import { useEffect } from "react";
import { Col, Container, Row, Nav, Button } from "react-bootstrap";
import { useProductContext } from "../hooks/useProductContext";
import { Link, NavLink } from "react-router-dom";

import productImg1 from "../images/product-img-test.jpg";
import productImg2 from "../images/product-img-test2.jpg";
import productImg3 from "../images/product-img-test3.jpg";
import productImg4 from "../images/product-img-test4.jpg";
import productImg5 from "../images/product-img-test5.jpg";

const NewProducts = () => {
  const { products, dispatch } = useProductContext();

  const productImages = [
    productImg1,
    productImg2,
    productImg3,
    productImg4,
    productImg5,
  ];

  useEffect(() => {
    const fetchActiveProducts = async () => {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`);

      const json = await response.json();
      if (response.ok) {
        dispatch({ type: "SET_PRODUCTS", payload: json });
      }
    };

    fetchActiveProducts();
  }, [dispatch]);
  return (
    <Container className="new-products ">
      <Container >
        <Row>
          {products &&
            products.slice(0, 4).map((product) => (
              <Col className="col-3 newActiveProduct lg-font-body" key={product._id}>
                <Nav.Link
                  as={NavLink}
                  to={`/products/${product._id}`}
                  product={product}
                >
                  <img
                    src={
                      productImages[
                        Math.floor(Math.random() * productImages.length)
                      ]
                    }
                    alt="product-image"
                  />
                  <h4>{product.productName}</h4>
                  <p>&#8369; {product.price}</p>
                </Nav.Link>
              </Col>
            ))}
        </Row>

        <Container className="d-flex justify-content-center">
          <NavLink to="/products">
            <Button className="dark-primary-btn">View collection</Button>
          </NavLink>
        </Container>
      </Container>
    </Container>
  );
};

export default NewProducts;
