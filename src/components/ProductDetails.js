import ProductModal from "./ProductModal";

import formatDistanceToNow from "date-fns/formatDistanceToNow";
import Switch from "./Switch";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import { useState } from "react";

const ProductDetails = ({ product }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <Container fluid className="admin-products">
      <Row className="admin-product-details">
        <Col className="col-8">
          <h4>{product.productName}</h4>
          <p>{product.description}</p>
          <p>&#8369; {product.price}</p>
          <p>
            {!product.updatedOn
              ? `Created ${formatDistanceToNow(new Date(product.createOn), {
                  addSuffix: true,
                })}`
              : `Updated ${formatDistanceToNow(new Date(product.updatedOn), {
                  addSuffix: true,
                })}`}
          </p>
        </Col>
        <Col className="col-4">
          <Row className="admin-product-action">
            <Col className="col-3">
              <span className="material-symbols-outlined " onClick={handleShow}>
                edit
              </span>
            </Col>
            <Col className="col-9">
              <Switch
                productId={product._id}
                value={product.isActive}
                onChange={(value) => console.log(value)}
              />
            </Col>
          </Row>
        </Col>
      </Row>

      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header closeButton className="p-4">
          <Modal.Title>
            <h2>Update Product</h2>
          </Modal.Title>
        </Modal.Header>
        <ProductModal product={product} handleClose={handleClose} />
      </Modal>
      {/* <ProductModal product={product}/> */}
    </Container>
  );
};

export default ProductDetails;
