import { useState, useEffect } from "react";
import { Form, Button, Container, FloatingLabel } from "react-bootstrap";

import { useUpdateProduct } from "../hooks/useUpdateProduct";

const ProductModal = ({ product, handleClose }) => {
  const [productName, setProductName] = useState(product.productName);
  const [description, setDescription] = useState(product.description);
  const [price, setPrice] = useState(product.price);
  const [isActive, setIsActive] = useState(false);

  const updateProduct = useUpdateProduct();

  const productId = product._id;
  useEffect(() => {
    if (
      productName !== product.productName ||
      description !== product.description ||
      price !== product.price
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, price]);

  const updateHandle = async (e) => {
    e.preventDefault();

    setIsActive(false);
    const productDetails = { productId, productName, description, price };

    await updateProduct(productDetails);

    handleClose()
  };
  return (
    <Container>
      <Form onSubmit={updateHandle} className="p-3 my-3">
        <Form.Group className="mb-3">
          <Form.Label>Product Name:</Form.Label>
          <FloatingLabel label={productName} className="mb-3">
            <Form.Control
              type="text"
              placeholder={productName}
              onChange={(e) => setProductName(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Price:</Form.Label>
          <FloatingLabel label={price} className="mb-3">
            <Form.Control
              type="number"
              inputMode="number"
              placeholder={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Description:</Form.Label>
          <FloatingLabel label={description}>
            <Form.Control
              as="textarea"
              placeholder={description}
              style={{ height: "100px" }}
              onChange={(e) => setDescription(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>


        <Button
          variant={isActive ? "success " : "danger"}
          type="submit"
          id="submitBtn"
          disabled={isActive ? false : true}
          className="dark-primary-btn mt-3"
        >
          Update
        </Button>
      </Form>
    </Container>
    // <div className="workout-details">
    //   <h4>{product.productName}</h4>
    //   <p>{product.description}</p>
    //   <p>&#8369; {product.price}</p>
    //   <p>
    //     Created{" "}
    //     {formatDistanceToNow(new Date(product.createOn), { addSuffix: true })}
    //   </p>
    // </div>
  );
};

export default ProductModal;
