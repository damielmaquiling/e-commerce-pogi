import { useState } from "react";
import {
  Container,
  Navbar,
  Nav,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

const AdminSidebar = () => {
    const [active, setActive] = useState(0)

    const handleClick = (index) => {
        setActive(index);
      };
  return (
    <aside className="main-menu">
      <div className="sidebar-links">
        <ul>
          <li>
            <NavLink to="/">
              <p className={active === 0 ? 'nav-text-active' : 'nav-text'} onClick={() => handleClick(0)}>
                <span className="material-symbols-outlined">
                  space_dashboard
                </span>
                Dashboard
              </p>
            </NavLink>
          </li>
          <li>
            <NavLink to="/">
              <p className={active === 1 ? 'nav-text-active' : 'nav-text'} onClick={() => handleClick(1)}>
                <span className="material-symbols-outlined">group</span>User
                Orders
              </p>
            </NavLink>
          </li>
          <li>
            <NavLink to="/">
              <p className={active === 2 ? 'nav-text-active' : 'nav-text'} onClick={() => handleClick(2)}>
                <span className="material-symbols-outlined">
                  manage_accounts
                </span>
                Set Admin
              </p>
            </NavLink>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default AdminSidebar;
