import { useState } from "react";
import { useProductContext } from "../hooks/useProductContext";
import { useAuthContext } from "../hooks/useAuthContext";
import Swal from "sweetalert2";
import { Container, Row, Form, Button, FloatingLabel } from "react-bootstrap";

const ProductForm = () => {
  const { dispatch } = useProductContext();
  const { user } = useAuthContext();

  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    const product = { productName, description, price };
    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: "POST",
      body: JSON.stringify(product),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    });
    const json = await response.json();

    if (response.ok) {
      Swal.fire({
        title: `${json.productName} is Added`,
        icon: "success",
        text: "A new product is added",
      });
      dispatch({ type: "CREATE_PRODUCTS", payload: json });
      setProductName("");
      setDescription("");
      setPrice("");
    }
  };

  return (
    <Container className="create">
      <form onSubmit={handleSubmit}>
        <h3>Add a New Product</h3>
        <FloatingLabel label="Product Name" className="mb-3">
          <Form.Control
            type="text"
            placeholder="Product Name"
            required
            onChange={(e) => setProductName(e.target.value)}
            value={productName}
          />
        </FloatingLabel>

        <FloatingLabel label="Price" className="mb-3">
          <Form.Control
            type="number"
            inputMode="number"
            placeholder="Price"
            required
            onChange={(e) => setPrice(e.target.value)}
            value={price}
          />
        </FloatingLabel>

        <FloatingLabel label="Description">
          <Form.Control
            as="textarea"
            placeholder="Product Description"
            style={{ height: "100px" }}
            required
            onChange={(e) => setDescription(e.target.value)}
            value={description}
          />
        </FloatingLabel>
        <Button
          type="submit"
          className="dark-primary-btn"
        >
          Add Product
        </Button>
      </form>
    </Container>
  );
};

export default ProductForm;
