import { Col, Container, Row } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import facebook from "../images/Logo--facebook.png";
import instagram from "../images/Logo--instagram.png";
import linkedin from "../images/Logo--linkedin.png";
import twitter from "../images/Logo--twitter.png";

const Footer = () => {
  return (
    <footer>
      <Container fluid>
        <div className="copyright">
          <p>&#169; Copyright 2023 Pogi-Damiel</p>
        </div>
        <div className="social-icons">
          <Link
            to="https://www.linkedin.com/in/damiel-maquiling-3073ab195/"
            target="_blank"
          >
            <img src={linkedin} alt="linkedin" />
          </Link>

          <Link to="https://www.facebook.com/blank.damiel/" target="_blank">
            <img src={facebook} alt="facebook" />
          </Link>

          <Link to="https://www.instagram.com/damieru_/" target="_blank">
            <img src={instagram} alt="instagram" />
          </Link>

          <Link to="https://twitter.com/makpepe31" target="_blank">
            <img src={twitter} alt="twitter" />
          </Link>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;
