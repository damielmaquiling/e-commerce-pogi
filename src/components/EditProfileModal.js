import { useState, useEffect } from "react";
import { Form, Button, Container, FloatingLabel } from "react-bootstrap";

import { useUpdateUserDetails } from "../hooks/useUpdateUserDetails";

const EditProfileModal = ({ userDetailsResult, handleClose }) => {
  const updateUserDetails = useUpdateUserDetails();

  const [firstName, setFirstName] = useState(userDetailsResult.firstName);
  const [lastName, setLastName] = useState(userDetailsResult.lastName);
  const [email, setEmail] = useState(userDetailsResult.email);
  const [mobileNum, setMobileNum] = useState(userDetailsResult.mobileNum);
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      firstName !== userDetailsResult.firstName ||
      lastName !== userDetailsResult.lastName ||
      email !== userDetailsResult.email ||
      mobileNum !== userDetailsResult.mobileNum
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNum]);

  const updateHandle = async (e) => {
    e.preventDefault();

    const userDetails = { firstName, lastName, email, mobileNum };
    
    await updateUserDetails(userDetails)

    handleClose();
  };
  return (
    <Container>
      <Form onSubmit={updateHandle} className="p-3 my-3">
        <Form.Group className="mb-3">
          <Form.Label>First Name:</Form.Label>
          <FloatingLabel label={firstName} className="mb-3">
            <Form.Control
              type="text"
              placeholder={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Last Name:</Form.Label>
          <FloatingLabel label={lastName} className="mb-3">
            <Form.Control
              type="text"
              placeholder={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Email:</Form.Label>
          <FloatingLabel label={email}>
            <Form.Control
              type="email"
              placeholder={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Mobile Number:</Form.Label>
          <FloatingLabel label={mobileNum}>
            <Form.Control
              type="text"
              placeholder={mobileNum}
              onChange={(e) => setMobileNum(e.target.value)}
            />
          </FloatingLabel>
        </Form.Group>

        <Button
          variant={isActive ? "success " : "danger"}
          type="submit"
          id="submitBtn"
          disabled={isActive ? false : true}
          className="dark-primary-btn mt-3"
        >
          Update
        </Button>
      </Form>
    </Container>
  );
};

export default EditProfileModal;
