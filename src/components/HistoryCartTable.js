import { Fragment } from "react";
import { Col, Row, Table } from "react-bootstrap";
import { format } from "date-fns";

import cartImg1 from "../images/cart-img1.jpg";
import cartImg2 from "../images/cart-img2.jpg";
import cartImg3 from "../images/cart-img3.jpg";
import cartImg4 from "../images/cart-img4.jpg";
import cartImg5 from "../images/cart-img5.jpg";

const HistoryCartTable = ({ orders }) => {
  const cartImages = [cartImg1, cartImg2, cartImg3, cartImg4, cartImg5];

  const randomIndex = Math.floor(Math.random() * cartImages.length);

  const historyorders = [orders];
  return (
    <Table>
      <thead>
        <tr>
          <th>Product</th>
          <th>Quantity</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody >
        {historyorders.map((order, orderIndex) => (
          <Fragment key={orderIndex}>
            {order.map((item, itemIndex) => (
              <tr key={`${orderIndex}-${itemIndex}`}>
                <td>
                  <div style={{ display: "flex" }}>
                    <img
                      src={
                        cartImages[
                          Math.floor(Math.random() * cartImages.length)
                        ]
                      }
                      alt="cart-image"
                    />
                    <div className="order-details">
                      <h4>{item.productName}</h4>
                      <p>{item.description}</p>
                      <p className="order-price">&#8369; {item.price}</p>
                    </div>
                  </div>
                </td>
                <td style={{ width: "300px" }}>
                  <p className="order-quantity">{item.quantity}</p>
                </td>
                <td>
                  <p className="order-subtotal">
                    &#8369; {item.quantity * item.price}
                  </p>
                  <p>
                    `Purchased on{" "}
                    {format(new Date(item.purchasedOn), "MMMM dd, yyyy")}`
                  </p>
                </td>
              </tr>
            ))}
          </Fragment>
        ))}
      </tbody>
    </Table>
  );
};

export default HistoryCartTable;
