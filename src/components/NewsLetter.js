import { Button, Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";

const NewsLetter = () => {
  return (
    <Container fluid className="news-letter">
      <Container className="news-letter-content d-flex flex-column align-items-center">
        <h2>Join the club and get the benefits</h2>
        <p>Sign up for our newsletter and receive exclusive offers on new ranges, sales, pop up stores and more</p>
        <NavLink to="/register">
                <Button className="dark-primary-btn ">Register now</Button>
              </NavLink>
      </Container>
    </Container>
  );
};

export default NewsLetter;
