import {
  Container,
  Navbar,
  Nav,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import { useAuthContext } from "../hooks/useAuthContext";
import { useLogout } from "../hooks/useLogout";

const AdminNavbar = () => {
  const { user } = useAuthContext();
  const logout = useLogout();

  return (
    <Navbar expand="lg" className="px-5 py-3 align-items-center border-bottom">
      <Container fluid>
        <Navbar.Brand as={Link} to="/" className="nav-brand-sm" sticky="top">
          <h3>Pogi</h3>
        </Navbar.Brand>
        <Nav className="ms-auto">
          <div className="d-flex align-items-center">
            <Navbar.Text className="material-symbols-outlined">
              notifications
            </Navbar.Text>
            <DropdownButton
              title={
                <span className="material-symbols-outlined">
                  account_circlearrow_drop_down
                </span>
              }
              hidden={user ? false : true}
            >
              <Dropdown.Item>{user && <>{user.email}</>}</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
            </DropdownButton>
          </div>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default AdminNavbar;
