import { Col, Container, Row, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import productImg1 from "../images/product-img-test.jpg";
import productImg2 from "../images/product-img-test2.jpg";
import productImg3 from "../images/product-img-test3.jpg";
import productImg4 from "../images/product-img-test4.jpg";
import productImg5 from "../images/product-img-test5.jpg";

const ActiveProductsDetails = ({ product }) => {
  const productImages = [
    productImg1,
    productImg2,
    productImg3,
    productImg4,
    productImg5,
  ];

  const randomIndex = Math.floor(Math.random() * productImages.length);
  const randomProductImage = productImages[randomIndex];
  return (
    <Col className="col-4 activeProduct lg-font-body">
      <Nav.Link as={NavLink} to={`/products/${product._id}`} product={product}>
        <img src={randomProductImage} alt="product-image" />
        <h4>{product.productName}</h4>
        <p>&#8369; {product.price}</p>
      </Nav.Link>
    </Col>
  );
};

export default ActiveProductsDetails;
