import { Col, Row, Table } from "react-bootstrap";
import React, { Fragment, useEffect, useState } from "react";
import { useUserCart } from "../hooks/useUserCart";
import { useAddQuantity } from "../hooks/useAddQuantity";
import { useSubQuantity } from "../hooks/useSubQuantity";
import debounce from "lodash/debounce";

import cartImg1 from "../images/cart-img1.jpg";
import cartImg2 from "../images/cart-img2.jpg";
import cartImg3 from "../images/cart-img3.jpg";
import cartImg4 from "../images/cart-img4.jpg";
import cartImg5 from "../images/cart-img5.jpg";

const CartTable = ({ orders, totalAmount, handleTableRender }) => {
  const { userCart } = useUserCart();
  const userCartAddQuantity = useAddQuantity();
  const userCartSubQuantity = useSubQuantity();
  const [addButtonClicked, setAddButtonClicked] = useState(false);

  const cartImages = [cartImg1, cartImg2, cartImg3, cartImg4, cartImg5];

  const randomIndex = Math.floor(Math.random() * cartImages.length);
  const randomProductImage = cartImages[randomIndex];

  const debouncedAddQuantity = debounce(async (newQuantity, productId) => {
    await userCartAddQuantity(newQuantity, productId);
    handleTableRender(newQuantity, productId)
  }, 500);

  const debouncedSubQuantity = debounce(async (newQuantity, productId) => {
    await userCartSubQuantity(newQuantity, productId);
    handleTableRender(newQuantity, productId)
  }, 500);

  const handleSubQuantity = (item) => {
    const newQuantity = item.quantity - 1;
    console.log(newQuantity)

    debouncedSubQuantity(newQuantity, item.productId);
  };

  const handleAddQuantity = (item) => {
    const newQuantity = item.quantity + 1;
    console.log(newQuantity)
    debouncedAddQuantity(newQuantity, item.productId);
  };


  return (
    <Table>
      <thead>
        <tr>
          <th>Product</th>
          <th>Quantity</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {orders.map((order, orderIndex) => (
          <Fragment key={orderIndex}>
            {order.map((item, itemIndex) => (
              <tr key={`${orderIndex}-${itemIndex}`}>
                <td>
                  <div style={{ display: "flex" }}>
                    <img src={randomProductImage} alt="cart-image" />
                    <div className="order-details">
                      <h4>{item.productName}</h4>
                      <p>{item.description}</p>
                      <p className="order-price">&#8369; {item.price}</p>
                    </div>
                  </div>
                </td>
                <td style={{ width: "300px" }}>
                  <Row className="quantity">
                    <Col onClick={() => handleSubQuantity(item)}>
                      <button className="material-symbols-outlined quantity-button">
                        remove
                      </button>
                    </Col>
                    <Col>
                      <p className="order-quantity">{item.quantity}</p>
                    </Col>
                    <Col onClick={() => handleAddQuantity(item)}>
                      <button className="material-symbols-outlined quantity-button">
                        add
                      </button>
                    </Col>
                  </Row>
                </td>
                <td>
                  <p className="order-subtotal">
                    &#8369; {item.quantity * item.price}
                  </p>
                </td>
              </tr>
            ))}
          </Fragment>
        ))}
      </tbody>
    </Table>
  );
};

export default CartTable;
