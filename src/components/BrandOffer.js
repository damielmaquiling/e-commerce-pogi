import { Col, Container, Row } from "react-bootstrap";
import brandIcon1 from "../images/brand-offer-1.png";
import brandIcon2 from "../images/brand-offer-2.png";
import brandIcon3 from "../images/brand-offer-3.png";
import brandIcon4 from "../images/brand-offer-4.png";

const BrandOffer = () => {
  return (
    <Container fluid className="brand-offer">
      <h3>What makes our brand different</h3>

      <Row>
        <Col>
          <img src={brandIcon1} alt="delivery" />
          <h4>Next day as standard</h4>
          <p>Order before 3pm and get your order the next day as standard</p>
        </Col>
        <Col>
          <img src={brandIcon2} alt="delivery" />
          <h4>Made by true artisans</h4>
          <p>Handmade crafted goods made with real passion and craftmanship</p>
        </Col>
        <Col>
          <img src={brandIcon3} alt="delivery" />
          <h4>Unbeatable prices</h4>
          <p>
            For our materials and quality you won’t find better prices anywhere
          </p>
        </Col>
        <Col>
          <img src={brandIcon4} alt="delivery" />
          <h4>Recycled packaging</h4>
          <p>
            We use 100% recycled packaging to ensure our footprint is manageable
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default BrandOffer;
