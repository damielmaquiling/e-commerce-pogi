import { useAuthContext } from "./useAuthContext";
import Swal from "sweetalert2";

export const useLogin = () => {
  const { dispatch } = useAuthContext();

  const login = async (email, password) => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email, password }),
    });

    const json = await response.json();

    if (!response.ok) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Check your login credentials and try again.",
      });
    }

    if (response.ok) {
      Swal.fire({
        title: "Login Successful",
        icon: "success",
        text: "Welcome Pogi!",
      });

      localStorage.setItem("user", JSON.stringify(json));
      dispatch({ type: "LOGIN", payload: json });
    }
  };

  return login;
};
