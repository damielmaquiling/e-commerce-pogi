import { useAuthContext } from "./useAuthContext";

export const useDecodeToken = () => {
  const { dispatch } = useAuthContext();

  const decodeToken = async (token) => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const json = await response.json();

    if (!response.ok) {
      console.log("Token error");
    }

    if (response.ok) {
      console.log("Token acess");
    }
  };

  return decodeToken;
};
