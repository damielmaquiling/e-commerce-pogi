import { useState } from "react";
import { format } from "date-fns";
import { useAuthContext } from "./useAuthContext";

export const useUserDetails = () => {
  const { user } = useAuthContext();

  const [userDetailsResult, setUserDetailsResult] = useState({});
  const [userJoined, setUserJoined] = useState("");

  const userDetails = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/user/userDetails`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    const json = await response.json();
    if (response.ok) {
      setUserDetailsResult(json);
      setUserJoined(format(new Date(json.createdAt), "MMMM dd, yyyy"));
    }
  };
  return { userDetails, userDetailsResult, userJoined };
};
