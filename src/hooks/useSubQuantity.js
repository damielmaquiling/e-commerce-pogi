import { useState, useEffect, useCallback } from "react";
import debounce from "lodash/debounce";

export const useSubQuantity = () => {
  const [pendingUpdate, setPendingUpdate] = useState(null);

  const userCartSubQuantity = useCallback(async (quantity, productId) => {
    setPendingUpdate({ quantity, productId });
  }, []);

  const debouncedUpdate = debounce(async () => {
    const { quantity, productId } = pendingUpdate;

    const user = JSON.parse(localStorage.getItem("user"));
    const token = user.token;

    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/myOrders`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const json = await response.json();

    if (response.ok) {
      const ordersData = await Promise.all(
        json.map(async (item) => {
          const productIdToFind = productId;
          const productToFind = item.products.find(
            (product) => product.productId === productIdToFind
          );
          if (!productToFind) {
            return null;
          }
          return {
            orderId: item._id,
            quantity: productToFind.quantity,
          };
        })
      );

      const orderToUpdate = ordersData.find((item) => item !== null);

      if (orderToUpdate) {
        const responseCalculate = await fetch(
          `${process.env.REACT_APP_API_URL}/user/myOrders/subQuantity/${productId}/1`,
          {
            method: "PATCH",
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const jsonCalculate = await responseCalculate.json();
        console.log(jsonCalculate);
      }
    }

    setPendingUpdate(null);
  }, 500);

  // Call the debounced update function whenever a new pending update is set.
  useEffect(() => {
    if (pendingUpdate) {
      debouncedUpdate();
    }
  }, [pendingUpdate, debouncedUpdate]);

  return userCartSubQuantity;
};
