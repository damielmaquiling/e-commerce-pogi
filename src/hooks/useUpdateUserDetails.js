import { useAuthContext } from "./useAuthContext";
import Swal from "sweetalert2";

export const useUpdateUserDetails = () => {
  const { user, dispatch } = useAuthContext();
  const updateUserDetails = async (userDetails) => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/user/userUpdateDetails`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify(userDetails),
      }
    );

    if (!response.ok)
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Something went wrong.",
      });

    if (response.ok)
      Swal.fire({
        title: "Update Successful",
        icon: "success",
        text: "User Profile is now updated",
      });
      
      dispatch({type: "UPDATE_USER", payload: {email: userDetails.email}})
  };
  return updateUserDetails;
};
