import Swal from "sweetalert2";

import { useAuthContext } from "./useAuthContext";

export const useCheckout = () => {
  const { user } = useAuthContext();

  const checkout = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/checkout/pay`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    });
    // const json = await response.json();

    if (!response.ok) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Something Went wrong",
      });
    }

    if (response.ok) {
      Swal.fire({
        title: "Checkout Complete",
        icon: "success",
        text: "Thank you",
      });
    }
  };
  return checkout;
};
