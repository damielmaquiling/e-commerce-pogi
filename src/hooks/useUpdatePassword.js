import { useAuthContext } from "./useAuthContext";
import Swal from "sweetalert2";

export const useUpdatePassword = () => {
  const { user, dispatch } = useAuthContext();

  const UpdatePassword = async (password) => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/user/userPasswordUpdate`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          password: password,
        }),
      }
    );

    if (!response.ok)
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Something went wrong.",
      });

    if (response.ok) {
      Swal.fire({
        title: "Please Re-Login",
        icon: "success",
        text: "Password is now updated",
      });
      localStorage.removeItem("user");
      dispatch({ type: "LOGOUT" });
    }
  };
  return UpdatePassword;
};
