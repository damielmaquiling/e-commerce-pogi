import { useAuthContext } from "./useAuthContext";
import Swal from "sweetalert2";

export const useAddToCart = () => {
  const { user } = useAuthContext();

  const addCart = async (productId, quantity) => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify({ productId: productId, quantity }),
    });
    // const json = await response.json();

    if (!response.ok) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Something Went wrong",
      });
    }

    if (response.ok) {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Product Added to cart',
        text: "Check your cart to checkout",
        showConfirmButton: false,
        timer: 1500
      })
    }
  };
  return addCart;
};
