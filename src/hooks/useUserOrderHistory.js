import { useState } from "react";
import { useAuthContext } from "./useAuthContext";

export const useUserOrderHistory = () => {
  const { user } = useAuthContext();
  
  const[orders, setOrders]= useState([])

  const userOrderHistory = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/user/ordersHistory`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    const json = await response.json();
    if (response.ok) {
      const ordersData = await Promise.all(
        json.map(async (item) => {
          const getProductName = await fetch(
            `${process.env.REACT_APP_API_URL}/products/${item.productId}`
          );

          const result = await getProductName.json()
          
          return {
            productId: item.productId,
            productName: result.productName,
            description: result.description,
            price: result.price,
            quantity: item.quantity,
            subtotal: item.subTotal,
            _id: item._id,
            purchasedOn: item.purchasedOn
          }
        })
      );
      setOrders(ordersData)
    }
  };
  return {userOrderHistory,orders};
};
