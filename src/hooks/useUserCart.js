// import { useAuthContext } from "./useAuthContext";

import { useState } from "react";

export const useUserCart = () => {
  //   const { user } = useAuthContext();
  const [totalAmount, setTotalAmount] = useState(0);
  const [orders, setOrders] = useState([]);

  const userCart = async () => {
    const user = JSON.parse(localStorage.getItem("user"));
    const token = user.token;
    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/myOrders`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const json = await response.json();
    if (response.ok) {
      const ordersData = await Promise.all(
        json.map(async (item) => {
          const order = await Promise.all(
            item.products.map(async (product) => {
              const id = product.productId;
              const getProductName = await fetch(
                `${process.env.REACT_APP_API_URL}/products/${id}`
              );
              const result = await getProductName.json();

              return {
                productId: id,
                productName: result.productName,
                description: result.description,
                price: result.price,
                quantity: product.quantity,
                subtotal: product.subTotal,
                _id: product._id,
              };
            })
          );

          return order;
        })
      );

      setOrders(ordersData)
      setTotalAmount(json[0].totalAmount);
      
    }
  };
  return {orders, setOrders, totalAmount, setTotalAmount, userCart};
};
